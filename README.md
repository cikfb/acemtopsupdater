# Main

Main simultaneously updates the files called "wfcmistr.prm" and "acemtops.ini" via user-specified instructions.
The user can choose to either (1)Add, (2)Update, or (3)Delete specific lines in the respective files. 

## Getting Started

This is a gradle project. In order to create a jar simply go to the gradle menu and click Tasks/build/jar.
The jar that gets created will be located in the directory build/libs. 

## Deployment

Once you have the jar, you'll need to put the "wfcmistr.prm" and "acemtops.ini" files in the same directory that the jar 
is located. Then, type the following command in the terminal to run the jar:

```
java -jar updateusers.jar [UpdateStatement] [UpdateType]
```

Main expects two command-line arguments which can be seen above (UpdateStatement and UpdateType).
After the jar is finished running, both files will be updated, and a log file called "UserUpdater_Log.0.log" will be 
created. This will detail any errors that were encountered during runtime.

### [UpdateStatement]:
The command-line argument, [UpdateStatement], has the following format:

[(Header Name)] (Account Number), (Account Name), (- or +), (Tender),, (1, 2, or 3), (Key Value from "wfcmistr.prm")

Note: If the [UpdateType] is delete, then you the [UpdateStatement] should be [(Header Name)] (Account Number), (Key Value from "wfcmistr.prm")

### [UpdateType]:
The command-line argument, [UpdateType], can be one of three options:

(1) "Add": Using the [UpdateStatement], this will add a new account with the given information. Also, adds the appropriate information to "wfcmistr.prm".
The program will throw an error if the account already exists in the acemtops.ini, or if the given header doesn't exist.

(2) "Update": Using the [UpdateStatement], this will update an existing account with the given information. Also, adds the appropriate information to "wfcmistr.prm".
The program will throw an error if the account doesn't exist in the acemtops.ini, or if there's no new changes to be made.

(3) "Delete": Using the [UpdateStatement], this will remove the given account from acemtops.ini, as well as the key value from "wfcmistr.prm".
The program will throw an error if the account doesn't exist in the acemtops.ini.

### Examples:

```
java -jar updateusers.jar "[Credit/Debit Deposits] 19,DISCOVER,-,53,,1,23" "add"
```

```
java -jar updateusers.jar "[Funds Received] 652,POSTAGE STAMP,-,53,,1,65" "update"
```

```
java -jar updateusers.jar "[Misc Income (From Terminal)] 565,713" "delete"
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Kyle Back (cikfb)** - *Initial work*

## License

This project is licensed under Wakefern's License - see the [LICENSE.md](LICENSE.md) file for details