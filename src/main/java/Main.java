import java.io.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main
{
    // Logger
    public static final LOG_HANDLER log_handler = new LOG_HANDLER("AcemtopsUpdater_Log", "");

    // Constants for possible update types
    private static final String ADD = "add";
    private static final String UPDATE = "update";
    private static final String DELETE = "delete";

    // Files that'll be traversed/edited during program execution
    private static File acemtops;
    private static File acemtopsTemp;
    private static File wfcmistr;
    private static File wfcmistrTemp;

    // Readers and Writers for the files
    private static BufferedReader acemtopsReader;
    private static BufferedReader wfcmistrReader;
    private static boolean headerEmpty = true;
    private static final int READ_AHEAD_LIMIT = 10000;

    // Keeps track of the account number/keyValue
    private static int account = 0;
    private static String keyValue = "";

    /**
     * Main method for Acemptops Updater.
     * @param args are the command-line arguments.
     */
    public static void main(String[] args)
    {
        log_handler.LOGGER.info("AcemtopsUpdater running...");

        // Checks if the user supplied the update statement
        if (args.length == 0)
        {
            exit("No update statement was provided.");
        }

        // Checks if the user supplied the type of update statement
        if (args.length == 1)
        {
            exit("Type of the update statement was not provided.");
        }

        // Makes sure the user didn't supply too many arguments
        if (args.length > 2)
        {
            exit("Too many arguments. Please only provide an update statement and the type.");
        }

        String updateStatement = checkUpdateStatement(args[0]);

        if (updateStatement == null)
        {
            exit("Update statement improperly formatted.");
        }

        String updateType = checkUpdateType(args[1]);

        if (updateType == null)
        {
            exit("Update type improperly formatted.");
        }

        // Initializes all the files (The temp files will get edited, then the original
        // files will get replaced with them once all updates are finished)
        acemtops = createNewFile(System.getProperty("user.dir") + "/acemtops.ini");
        acemtopsTemp = createNewFile(System.getProperty("user.dir") + "/temp.ini");
        clearFileContents(acemtopsTemp);
        wfcmistr = createNewFile(System.getProperty("user.dir") + "/wfcmistr.prm");
        wfcmistrTemp = createNewFile(System.getProperty("user.dir") + "/temp.prm");
        clearFileContents(wfcmistrTemp);

        try
        {
            // Isolates account/keyValue from updateStatement
            account = getAccount(updateStatement);
            keyValue = getKeyValue(updateStatement);

            // Initializes the buffered readers and writes for the program
            acemtopsReader = new BufferedReader(new FileReader(acemtops));
            wfcmistrReader = new BufferedReader(new FileReader(wfcmistr));

            // Performs the appropriate function based on the user's input
            switch (updateType)
            {
                case ADD:
                    addAcemtops(updateStatement);
                    addWfcmistr(updateStatement);
                    break;
                case UPDATE:
                    updateAcemtops(updateStatement);
                    updateWfcmistr(updateStatement);
                    break;
                case DELETE:
                    deleteAcemtops(updateStatement);
                    deleteWfcmistr(updateStatement);
                    break;
            }

            // Closes buffers
            acemtopsReader.close();
            wfcmistrReader.close();

            // Renames temp files to original files, then deletes them
            acemtops.delete();
            acemtopsTemp.renameTo(acemtops);
            wfcmistr.delete();
            wfcmistrTemp.renameTo(wfcmistr);
        }
        catch (Exception e)
        {
            closeBuffers();
            exit(getStackTrace(e));
        }

        log_handler.LOGGER.info("AcemtopsUpdater finished.");
    }

    /**
     * Adds a key value and the appropriate lines to wfcmistr.prm at the end of the file.
     * @param updateStatement is the statement provided by the user.
     * @throws Exception if any error occurs.
     */
    private static void addWfcmistr(String updateStatement) throws Exception
    {
        // Ensures the key value doesn't already exist
        if (existsInWfcmistr(keyValue))
        {
            closeBuffers();
            exit("Can't add \"" + keyValue + "\" because it already exists in wfcmistr.prm.");
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(wfcmistrTemp, true)))
        {
            String line = wfcmistrReader.readLine();

            // Copies contents from wfcmistr to the temp file
            while (line != null)
            {
                bw.write(line + "\n");
                line = wfcmistrReader.readLine();
            }

            // Add content to the temp file
            bw.write(keyValue + "\n");
            bw.write("A\n01\n03,");

            if (isDebit(updateStatement)) bw.write("-");

            bw.write("01," + prependZeroes(account) + ",");
        }
        catch (Exception e) { throw e; }
    }

    /**
     * Deletes the lines in wfcmistr.prm that correspond with the given key value.
     * @param updateStatement is the statement given by the user.
     * @throws Exception if any error occurs.
     */
    private static void deleteWfcmistr(String updateStatement) throws Exception
    {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(wfcmistrTemp, true)))
        {
            boolean keyValueFound = false;
            String line = wfcmistrReader.readLine();

            // Copies contents from wfcmistr to the temp file
            while (line != null)
            {
                // Checks if this is the keyValue to be deleted
                if (line.trim().equals(keyValue))
                {
                    keyValueFound = true;

                    // Skips past the next two lines
                    wfcmistrReader.readLine();
                    wfcmistrReader.readLine();

                    boolean nextKeyValue = false;

                    // Iterates through the lines until next key value encountered
                    while (!nextKeyValue)
                    {
                        line = wfcmistrReader.readLine();

                        // Next key value encountered
                        if (!line.contains(","))
                        {
                            nextKeyValue = true;
                        }
                    }
                }

                // Adds line to file
                bw.write(line + "\n");
                line = wfcmistrReader.readLine();
            }

            if (!keyValueFound)
            {
                closeBuffers();
                exit("Can't delete \"" + keyValue + "\" because it doesn't exist in wfcmistr.prm.");
            }
        }
        catch (Exception e) { throw e; }
    }

    /**
     * Updates wfcmistr.prm with the appropriate changes. The program will terminate
     * if the key value provided in the update statement doesn't exist.
     * @param updateStatement is the statement provided by the user.
     * @throws Exception if any error occurs.
     */
    private static void updateWfcmistr(String updateStatement) throws Exception
    {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(wfcmistrTemp, true)))
        {
            boolean keyValueFound = false;
            String line = wfcmistrReader.readLine();

            // Copies contents from wfcmistr to the temp file
            while (line != null)
            {
                // Checks if this is the keyValue to be updated
                if (line.trim().equals(keyValue))
                {
                    keyValueFound = true;

                    // Skips past the lines
                    wfcmistrReader.readLine();
                    wfcmistrReader.readLine();
                    wfcmistrReader.readLine();
                    line = wfcmistrReader.readLine();

                    // Add content to the temp file
                    bw.write(keyValue + "\n");
                    bw.write("A\n01\n03,");

                    if (isDebit(updateStatement)) bw.write("-");

                    bw.write("01," + prependZeroes(account) + ",\n");
                }

                // Adds line to file
                bw.write(line + "\n");
                line = wfcmistrReader.readLine();
            }

            if (!keyValueFound)
            {
                closeBuffers();
                exit("Can't update \"" + keyValue + "\" because it doesn't exist in wfcmistr.prm.");
            }
        }
        catch (Exception e) { throw e; }
    }

    /**
     * Prepends zeroes to an account number depending on the length of the account number.
     * It'll prepend enough zeroes to make the account number 4 digits.
     * @param account is the account that'll get the zeroes prepended to it.
     * @return the account with prepended zeroes.
     */
    private static String prependZeroes(int account)
    {
        String strAccount = account + "";

        // Determines how many zeroes need to be prepended
        int zeroesNeeded = 4 - strAccount.length();
        StringBuilder zeroes = new StringBuilder();

        // Appends appropriate amount of zeroes
        for (int i = 0; i < zeroesNeeded; i++)
        {
            zeroes.append("0");
        }

        // Concatenates the zeroes with account number
        return zeroes.toString() + strAccount;
    }

    /**
     * Determines if the second field in an update statement (Two after the account number) is debit ("-").
     * @param updateStatment is the statement where the second field is located.
     * @return true if the second field is debit, false if credit. The program will terminate if anything else.
     */
    private static boolean isDebit(String updateStatment)
    {
        String fields = updateStatment.substring(updateStatment.indexOf("]") + 1);
        Scanner sc = new Scanner(fields).useDelimiter(",");

        sc.next(); // Skip past account number
        sc.next(); // Skip past first field

        String secondField = sc.next().trim();
        sc.close();

        if (secondField.equals("-"))
        {
            return true;
        }
        else if (secondField.equals("+"))
        {
            return false;
        }
        else
        {
            closeBuffers();
            exit("Unrecognized field, \"" + secondField + "\". Expected either \"+\" or \"-\".");
            return false;
        }
    }

    /**
     * Isolates the key value from an update statement (The last value).
     * @param updateStatement is where the key value is contained.
     * @return the key value in string form.
     */
    private static String getKeyValue(String updateStatement)
    {
        return updateStatement.substring(updateStatement.lastIndexOf(",") + 1).trim();
    }

    /**
     * Determines if a given key value exists in wfcmistr.prm.
     * @param keyValue is the value that's being searched for.
     * @return true if the value exists in the file, false otherwise.
     * @throws Exception if any error occurs.
     */
    private static boolean existsInWfcmistr(String keyValue) throws Exception
    {
        // Marks this point in the buffered reader so we can come back to it.
        wfcmistrReader.mark(READ_AHEAD_LIMIT);

        // First line of the wfcmistr file
        String line = wfcmistrReader.readLine();

        // Iterates through wfcmistr file
        while (line != null)
        {
            line = line.trim();

            if (line.equals(keyValue))
            {
                // Resets reader back to start of file
                wfcmistrReader.reset();
                return true;
            }

            line = wfcmistrReader.readLine();
        }

        // Resets reader back to start of file
        wfcmistrReader.reset();
        return false;
    }

    /**
     * Adds the updateStatement to the acemtops file.
     * @param updateStatement is the statement provided by the user.
     * @throws Exception if any error occurs.
     */
    private static void addAcemtops(String updateStatement) throws Exception
    {
        // Isolates header, and determines if it exits in acemtops
        String header = getHeader(updateStatement);
        boolean headerExists = headerExists(header);

        if (!headerExists)
        {
            closeBuffers();
            exit("The header, [" + header + "], does not exist in acemtops.ini.");
        }

        boolean accountExists = accountExists(account);

        if (accountExists)
        {
            closeBuffers();
            exit("The account, \"" + account + "\", already exists in acemtops.ini for [" + header + "]");
        }

        // Now, we check to see if the update statement has the correct number of fields.
        // To do this, we're going to look at one of the lines under the header to see how
        // many parameters are expected, and compare that with how many fields we have.
        if (!headerEmpty && !sameNumFields(updateStatement))
        {
            closeBuffers();
            exit("Number of fields in update statement does not match number of fields in acemptops.ini.");
        }

        // Get to appropriate line where we're going to add
        String line = acemtopsReader.readLine();
        int currAccount;

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(acemtopsTemp, true)))
        {
            while (line != null)
            {
                // Makes sure the current line isn't blank
                if (line.trim().equals(""))
                {
                    continue;
                }

                // Checks to see if the next header in the file has been reached
                boolean endOfHeader = regex("^\\[.*\\]", line.trim());

                if (endOfHeader)
                {
                    // Add account, then current line, and a blank line
                    bw.write(account + " = " + getRemainingFields(updateStatement) + "\n");
                    bw.write(line + "\n");
                    bw.write("\n");
                    line = acemtopsReader.readLine();
                    break;
                }
                else
                {
                    currAccount = Integer.parseInt(line.substring(0, line.trim().indexOf('=')).trim());

                    if (currAccount > account)
                    {
                        // Add account, then current line
                        bw.write(account + " = " + getRemainingFields(updateStatement) + "\n");
                        bw.write(line + "\n");
                        line = acemtopsReader.readLine();
                        break;
                    }
                    else
                    {
                        // Add current line
                        bw.write(line + "\n");
                    }
                }

                // Iterates to next line
                line = acemtopsReader.readLine();
            }

            // At this point the new line has been added, so we need to
            // add the remaining lines from the file
            while (line != null)
            {
                bw.write(line + "\n");
                line = acemtopsReader.readLine();
            }

        }
        catch (Exception e) { throw e; }
    }

    /**
     * Updates a given account under a specific header in acemtops.ini.
     * @param updateStatement is the statement provided by the user.
     * @throws Exception if any error occurs.
     */
    private static void updateAcemtops(String updateStatement) throws Exception
    {
        // Isolates header, and determines if it exits in acemtops
        String header = getHeader(updateStatement);
        boolean headerExists = headerExists(header);

        if (!headerExists)
        {
            closeBuffers();
            exit("The header, [" + header + "], does not exist in acemtops.ini.");
        }

        boolean accountExists = accountExists(account);

        if (!accountExists)
        {
            closeBuffers();
            exit("The account, \"" + account + "\", doesn't exist in acemtops.ini for [" + header + "]");
        }

        // Now, we check to see if the update statement has the correct number of fields.
        // To do this, we're going to look at one of the lines under the header to see how
        // many parameters are expected, and compare that with how many fields we have.
        if (!headerEmpty && !sameNumFields(updateStatement))
        {
            closeBuffers();
            exit("Number of fields in update statement does not match number of fields in acemptops.ini.");
        }

        // Get to appropriate line where we're going to add
        String line = acemtopsReader.readLine();
        int currAccount;

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(acemtopsTemp, true)))
        {
            while (line != null)
            {
                // Makes sure the current line isn't blank
                if (line.trim().equals(""))
                {
                    line = acemtopsReader.readLine();
                    continue;
                }

                // Checks to see if the next header in the file has been reached
                boolean endOfHeader = regex("^\\[.*\\]", line.trim());

                if (endOfHeader)
                {
                    System.out.println("Something went wrong");
                    break;
                }
                else
                {
                    currAccount = Integer.parseInt(line.substring(0, line.trim().indexOf('=')).trim());

                    if (currAccount == account)
                    {
                        String remainingFields = getRemainingFields(updateStatement);
                        String lineFields = line.substring(line.indexOf("=") + 1).trim();

                        // Ensures that an update needs to be made first
                        if (sameFields(remainingFields, lineFields))
                        {
                            closeBuffers();
                            exit("The update you're trying to make already exists in Acemtops.ini.");
                        }

                        // Add updated account
                        bw.write(account + " = " + getRemainingFields(updateStatement) + "\n");
                        line = acemtopsReader.readLine();
                        break;
                    }
                    else
                    {
                        // Add current line
                        bw.write(line + "\n");
                    }
                }

                // Iterates to next line
                line = acemtopsReader.readLine();
            }

            // At this point the new line has been added, so we need to
            // add the remaining lines from the file
            while (line != null)
            {
                bw.write(line + "\n");
                line = acemtopsReader.readLine();
            }
        }
        catch (Exception e) { throw e; }
    }

    /**
     * Deletes the given account from acemptops.ini under the specfic header.
     * @param updateStatement is the statement provided by the user.
     * @throws Exception if any error occurs.
     */
    private static void deleteAcemtops(String updateStatement) throws Exception
    {
        // Isolates header, and determines if it exits in acemtops
        String header = getHeader(updateStatement);
        boolean headerExists = headerExists(header);

        if (!headerExists)
        {
            closeBuffers();
            exit("The header, [" + header + "], does not exist in acemtops.ini.");
        }

        // Get to appropriate line where we're going to add
        String line = acemtopsReader.readLine();
        int currAccount;

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(acemtopsTemp, true)))
        {
            while (line != null)
            {
                // Makes sure the current line isn't blank
                if (line.trim().equals(""))
                {
                    line = acemtopsReader.readLine();
                    continue;
                }

                // Checks to see if the next header in the file has been reached
                boolean endOfHeader = regex("^\\[.*\\]", line.trim());

                if (endOfHeader)
                {
                    closeBuffers();
                    exit("The account, \"" + account + "\", doesn't exist in acemtops.ini for [" + header + "]");
                }
                else
                {
                    currAccount = Integer.parseInt(line.substring(0, line.trim().indexOf('=')).trim());

                    if (currAccount == account)
                    {
                        // Skip current line
                        line = acemtopsReader.readLine();
                        break;
                    }
                    else
                    {
                        // Add current line
                        bw.write(line + "\n");
                    }
                }

                // Iterates to next line
                line = acemtopsReader.readLine();
            }

            // At this point the account has been removed, so we need to
            // add the remaining lines from the file
            while (line != null)
            {
                bw.write(line + "\n");
                line = acemtopsReader.readLine();
            }
        }
        catch (Exception e) { throw e; }
    }

    /**
     * Checks the fields supplied by the user (Excluding the account number), and the
     * fields of a given line in Acemtops.ini (Excluding the account number) to see if
     * they are identical.
     * @param userFields are the user-supplied fields.
     * @param fileFields are the fields on the current line.
     * @return true if all of the fields are the same, false otherwise.
     */
    private static boolean sameFields(String userFields, String fileFields)
    {
        // Create comma delimited scanners for the fields
        Scanner sc1 = new Scanner(userFields).useDelimiter(",");
        Scanner sc2 = new Scanner(fileFields).useDelimiter(",");

        while (sc1.hasNext())
        {
            // Different number of fields -> Not the same
            if (!sc2.hasNext()) return false;

            // Compares the current fields
            if (!sc1.next().trim().equals(sc2.next().trim()))
            {
                return false;
            }
        }

        // Closes the scanners
        sc1.close();
        sc2.close();

        // All the fields are the same
        return true;
    }

    /**
     * Removes all the contents of a file.
     * @param file is what's being emptied.
     */
    private static void clearFileContents(File file)
    {
        try (PrintWriter pw = new PrintWriter(file))
        {
            pw.print("");
        }
        catch (IOException e)
        {
            exit(getStackTrace(e));
        }
    }

    /**
     * Gets the fields from the update statement excluding the account number (The first
     * field), and the last field which is used for the wfcmistr.prm file.
     * @param updateStatement is the string from which we're isolating the fields.
     * @return the the isolated fields (Including the comma at the end).
     */
    private static String getRemainingFields(String updateStatement)
    {
        return updateStatement.substring(updateStatement.indexOf(',') + 1, updateStatement.lastIndexOf(',') + 1);
    }

    /**
     * Checks if the number of fields contained in an update statement is the same as
     * the number of fields contained on the current line of the acemtopsReader.
     * @param updateStatement is the statement that's being compared.
     * @return true if they have the same number fields, false otherwise.
     */
    private static boolean sameNumFields(String updateStatement) throws IOException
    {

        acemtopsReader.mark(READ_AHEAD_LIMIT);
        String noHeaderStatement = updateStatement.substring(updateStatement.indexOf(']') + 1);
        long commaCount1 = noHeaderStatement.chars().filter(ch -> ch == ',').count();
        long commaCount2 = acemtopsReader.readLine().chars().filter(ch -> ch == ',').count();
        acemtopsReader.reset();
        return (commaCount1 - 1) == commaCount2;
    }

    /**
     * Isolated the account number from an update statement. The account number is
     * expected to be the first field after the header.
     * @param updateStatement a string where the account number is being taken from.
     * @return the account number as an integer.
     */
    private static int getAccount(String updateStatement)
    {
        // Isolates the fields from the header in the update statemet
        String fields = updateStatement.substring(updateStatement.indexOf(']') + 1);

        // Delimits the fields using commas
        Scanner sc = new Scanner(fields).useDelimiter(",");
        String accountStr = sc.hasNext() ? sc.next().trim() : "";
        sc.close();

        try
        {
            int accountNum = Integer.parseInt(accountStr);

            // Makes sure the account number isn't too large
            if (accountNum > 9999)
            {
                closeBuffers();
                exit("The account number, \"" + accountStr + "\", should not contain more than 4 digits.");
            }

            return accountNum;
        }
        catch (NumberFormatException e)
        {
            closeBuffers();
            exit("Account number, \"" + accountStr + "\", is not an integer.");
            return -1;
        }
    }

    /**
     * Determines if a given account exists for a specfic header in acemtops.ini.
     * @param account is the integer account number that's being searched for.
     * @return true if the account exists, false otherwise.
     * @throws IOException if an error occurs while traversing the file.
     */
    private static boolean accountExists(int account) throws IOException
    {
        // Marks this point in the buffered reader so we can come back to it.
        acemtopsReader.mark(READ_AHEAD_LIMIT);

        // First line of the acemtops file
        String line = acemtopsReader.readLine();

        // Iterates through acemtops file
        while (line != null)
        {
            line = line.trim();

            // Checks to see if the next header in the file has been reached
            boolean endOfHeader = regex("^\\[.*\\]", line);

            if (endOfHeader)
            {
                // Resets reader back to line after header
                acemtopsReader.reset();
                return false;
            }

            if (!line.equals(""))
            {
                // There's at least one line under the header
                // (This will be used later in the program)
                headerEmpty = false;
            }

            // Determines if the current line contains the account number
            boolean containsAccount = regex("^" + account, line);

            if (containsAccount)
            {
                // Resets reader back to line after header
                acemtopsReader.reset();
                return true;
            }

            line = acemtopsReader.readLine();
        }

        // Resets reader back to line after header
        acemtopsReader.reset();
        return false;
    }

    /**
     * Closes the buffered readers and deletes temp files for the program.
     * Exits the program if an error is encountered.
     */
    private static void closeBuffers()
    {
        try
        {
            acemtopsTemp.delete();
            wfcmistrTemp.delete();
            acemtopsReader.close();
            wfcmistrReader.close();
        }
        catch (Exception e)
        {
            exit(getStackTrace(e));
        }
    }

    /**
     * Isolates the header contained within an update statement. The header is
     * at the start of an update statements and is wrapped around brackets.
     * @param updateStatement is the statement where the header is contained.
     * @return the header.
     */
    private static String getHeader(String updateStatement)
    {
        // Gets the indices of the brackets that contain the header
        int headerStart = updateStatement.indexOf('[') + 1;
        int headerEnd = updateStatement.indexOf(']');

        // Returns the header
        return updateStatement.substring(headerStart, headerEnd);
    }

    /**
     * Checks if a header exists within the Acemtops file.
     * @param header is the header that's being checked.
     * @return true if it's contained in the file, false otherwise.
     * @throws IOException if an error occurs while traversing the file.
     */
    private static boolean headerExists(String header) throws IOException
    {
        // First line of the acemtops file
        String line = acemtopsReader.readLine();

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(acemtopsTemp, true)))
        {
            // Iterates through acemtops file
            while (line != null)
            {
                bw.write(line + "\n");
                line = line.trim();

                // Determines if the current line contains the header
                boolean containsHeader = regex("^\\[" + header + "\\]", line);

                if (containsHeader)
                {
                    return true;
                }

                line = acemtopsReader.readLine();
            }
        }
        catch (Exception e) { throw e; }

        return false;
    }

    /**
     * Uses regex to determine if a certain pattern is contained within a string.
     * @param pattern is the pattern that's being matched.
     * @param str is the string that's being checked.
     * @return true if the pattern is matched within str, false otherwise.
     */
    private static boolean regex(String pattern, String str)
    {
        Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);
        return m.find();
    }

    /**
     * Adds a warning message to the log file, and exits the program.
     * @param warningMessage is the message that'll be added to the log.
     */
    private static void exit(String warningMessage)
    {
        log_handler.LOGGER.warning(warningMessage);
        log_handler.LOGGER.info("AcemtopsUpdater finished.");
        System.exit(0);
    }

    /**
     * Checks to make sure the update statement provided by the user is properly formatted.
     * @param updateStatement is the first command-line argument for the program.
     * @return the updateStatement if properly formatted, null otherwise.
     */
    private static String checkUpdateStatement(String updateStatement)
    {
        // Trims the update statement
        String trimmedStatement = updateStatement.trim();

        // Checks if the statement starts with "[*HEADER_NAME*]"
        boolean formatCorrect = regex("^\\[.*\\]", trimmedStatement);

        // Returns null if format is incorrect, the trimmed statement if correct
        return formatCorrect ? trimmedStatement : null;
    }

    /**
     * Checks to make sure the update type provided by the user is properly formatted.
     * @param updateType is the second command-line argument for the program.
     * @return the trimmed version of the update type if formatted properly, null otherwise.
     */
    private static String checkUpdateType(String updateType)
    {
        // Trims the updateType supplied by the user
        String trimmedType = updateType.toLowerCase().trim();

        // Makes sure it's a predefined type (Add, update, or delete)
        if (trimmedType.equals(ADD) || trimmedType.equals(UPDATE) || trimmedType.equals(DELETE))
        {
            return trimmedType;
        }
        else
        {
            return null;
        }
    }

    /**
     * Creates a new file where the updated user information will be written.
     * @return the newly created File.
     */
    private static File createNewFile(String path)
    {
        // The file will be located in the directory where the jar is running
        File newFile = new File(path);

        try
        {
            // Creates the file if it doesn't already exist
            newFile.createNewFile();
        }
        catch (java.io.IOException e)
        {
            exit(getStackTrace(e));
        }

        return newFile;
    }

    /**
     * Converts an exception to a stack trace in string form.
     * @param e is the exception being converted.
     * @return the string form of the stack trace.
     */
    private static String getStackTrace(Exception e)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }
}
